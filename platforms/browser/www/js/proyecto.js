function calcularIMC() {
  var peso = document.getElementById('peso').value;
  var estatura = document.getElementById('estatura').value;

  var combo_estatura = document.getElementById('medida_estatura');
  var combo_peso = document.getElementById('medida_peso');

  switch (combo_estatura.options[combo_estatura.selectedIndex].value) {
    case "1":
      estatura = estatura / 100;
      break;
    case "2":
      break;
    case "3":
      estatura = estatura / 39.3701;
      break;
    default:
      break;
  }

  switch (combo_peso.options[combo_peso.selectedIndex].value) {
    case "1":
      break;
    case "2":
      peso = peso / 2.20462;
      break;
    default:
      break;

  }

  var imc = peso / (estatura * estatura);
  document.getElementById('calculo').innerHTML = "I.M.C: "+parseFloat(imc).toFixed(2);
  clasificarIMC(imc);
  return false;
}

function clasificarIMC(imc) {
  var clasificacion = "";
  var fila = 99;

  switch (true) {
    case imc < 16:
      //clasificacion = "Delgadez Severa";
      fila = 1;
      break;
    case imc >= 16 && imc < 17:
      //clasificacion = "Delgadez moderada";
      fila = 2;
      break;
    case imc >= 17 && imc < 18.50:
      //clasificacion = "Delgadez aceptable";
      fila = 3;
      break;
    case imc >= 18.50 && imc < 25:
      //clasificacion = "Peso Normal";
      fila = 4;
      break;
    case imc >= 25 && imc < 30:
      //clasificacion = "Sobrepeso";
      fila = 5;
      break;
    case imc >= 30 && imc < 35:
      //clasificacion = "Obeso: Tipo I";
      fila = 6;
      break;
    case imc >= 35 && imc < 40:
      //clasificacion = "Obeso: Tipo II";
      fila = 7;
      break;
    case imc >= 40:
      //clasificacion = "Obeso: Tipo III";
      fila = 8;
      break;
    default:
      break;
  }

  if (fila < 9) {
    document.getElementById('tablaIMC').rows[fila].cells[1].style.backgroundColor = "#33FF33";
    document.getElementById('tablaIMC').rows[fila].cells[0].style.backgroundColor = "#66FF66";

    //document.getElementById('clasificacion_imc').innerHTML = clasificacion;
  }
  return false;
}
