function calcularIMC() {
  tablaNueva();
  var peso = document.getElementById('peso').value;
  var estatura = document.getElementById('estatura').value;

  var combo_estatura = document.getElementById('medida_estatura');
  var combo_peso = document.getElementById('medida_peso');

  switch (combo_estatura.options[combo_estatura.selectedIndex].value) {
    case "1":
      estatura = estatura / 100;
      break;
    case "2":
      break;
    case "3":
      estatura = estatura / 39.3701;
      break;
    default:
      break;
  }

  switch (combo_peso.options[combo_peso.selectedIndex].value) {
    case "1":
      break;
    case "2":
      peso = peso / 2.20462;
      break;
    default:
      break;

  }

  var imc = peso / (estatura * estatura);
  document.getElementById('calculo').innerHTML = "<h4>I.M.C: " + parseFloat(imc).toFixed(2)+"</h4>";
  clasificarIMC(imc);
  return false;
}

function clasificarIMC(imc) {
  var clasificacion = "";
  var fila = 99;

  switch (true) {
    case imc < 16:
      //clasificacion = "Delgadez Severa";
      fila = 1;
      break;
    case imc >= 16 && imc < 17:
      //clasificacion = "Delgadez moderada";
      fila = 2;
      break;
    case imc >= 17 && imc < 18.50:
      //clasificacion = "Delgadez aceptable";
      fila = 3;
      break;
    case imc >= 18.50 && imc < 25:
      //clasificacion = "Peso Normal";
      fila = 4;
      break;
    case imc >= 25 && imc < 30:
      //clasificacion = "Sobrepeso";
      fila = 5;
      break;
    case imc >= 30 && imc < 35:
      //clasificacion = "Obeso: Tipo I";
      fila = 6;
      break;
    case imc >= 35 && imc < 40:
      //clasificacion = "Obeso: Tipo II";
      fila = 7;
      break;
    case imc >= 40:
      //clasificacion = "Obeso: Tipo III";
      fila = 8;
      break;
    default:
      break;
  }

  if (fila < 9) {
    document.getElementById('tablaIMC').rows[fila].cells[1].style.backgroundColor = "#33FF33";
    document.getElementById('tablaIMC').rows[fila].cells[0].style.backgroundColor = "#66FF66";

    //document.getElementById('clasificacion_imc').innerHTML = clasificacion;
  }
  return false;
}

function tablaNueva(){
  var  tabla = "<h3>Clasificacion</h3>";
  tabla = tabla +"<table id='tablaIMC'>";
  tabla = tabla + "<thead><tr><th>Indice</th><th>Clasificacion</th></tr></thead>";
  tabla = tabla + "<tr><td>       < 16.00 </td><td> Infrapeso: Delgadez Severa    </td></tr>";
  tabla = tabla + "<tr><td> 16.00 - 16.99 </td><td> Infrapeso: Delgadez moderada  </td></tr>";
  tabla = tabla + "<tr><td> 17.00 - 18.49 </td><td> Infrapeso: Delgadez aceptable </td></tr>";
  tabla = tabla + "<tr><td> 18.50 - 24.99 </td><td> Peso Normal                   </td></tr>";
  tabla = tabla + "<tr><td> 25.00 - 29.99 </td><td> Sobrepeso                     </td></tr>";
  tabla = tabla + "<tr><td> 30.00 - 34.99 </td><td> Obeso: Tipo I                 </td></tr>";
  tabla = tabla + "<tr><td> 35.00 - 40.00 </td><td> Obeso: Tipo II                </td></tr>";
  tabla = tabla + "<tr><td>       > 40.00 </td><td> Obeso: Tipo III               </td></tr>";
  tabla = tabla + "<tbody>";
  tabla = tabla + "</tbody></table>";
  document.getElementById('clasificacion_imc').innerHTML = tabla;
}

function limpiarTabla(){
  document.getElementById('clasificacion_imc').innerHTML = "";
  document.getElementById('calculo').innerHTML = "";
  var peso = document.getElementById('peso').value = "";
  var estatura = document.getElementById('estatura').value = "";
}
